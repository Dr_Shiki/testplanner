#include <rclcpp/rclcpp.hpp>
#include <nav_msgs/msg/occupancy_grid.hpp>
#include <nav_msgs/msg/path.hpp>
#include <geometry_msgs/msg/pose_with_covariance_stamped.hpp>
#include <geometry_msgs/msg/point_stamped.hpp>

#include <chrono>
#include <sstream>
#include <iostream>
#include <functional>

#include <planner2D/Common.hpp>
#include <planner2D/ImageHandler.hpp>
#include <planner2D/msgsHandler.hpp>
#include <planner2D/Planner2D.hpp>


ImageHandler imgHandler("imgSource.png",140);
Planner2D planner(imgHandler.occGrid);

std::string FrameName = "locker";
class OwnPublisher : public rclcpp::Node
{
    public:
    OwnPublisher(): Node("planner2D")
    {
        this->declare_parameter<std::string>("PlannerName","RRT");
        _paramTimer = this->create_wall_timer(std::chrono::duration<float,std::ratio<1,1>>(std::chrono::seconds(1)),std::bind(&OwnPublisher::_checkParam,this));

        _ptr = this->create_publisher<nav_msgs::msg::OccupancyGrid>("/navGridTopic",10);
        _pathPtr = this->create_publisher<nav_msgs::msg::Path>("/navPathTopic",10);
        _startPointPtr = this->create_publisher<geometry_msgs::msg::PointStamped>("/startPoint",10);
        _finishPointPtr = this->create_publisher<geometry_msgs::msg::PointStamped>("/finishPoint",10);

        _initialPoint = this->create_subscription<geometry_msgs::msg::PoseWithCovarianceStamped>("/initialpose",10,std::bind(&OwnPublisher::_initPointCallBack,this,std::placeholders::_1));
        _finalPoint = this->create_subscription<geometry_msgs::msg::PoseStamped>("/goal_pose",10,std::bind(&OwnPublisher::_finalPointCallBack,this,std::placeholders::_1));
        
        
        _time = this->create_wall_timer(std::chrono::duration<float,std::ratio<1,1>>(std::chrono::seconds(7)),std::bind(&OwnPublisher::publishMap,this));
    }
    void publishMap()
    {
        OccupancyMsg occMsg(FrameName,imgHandler.getRowMajorArr(),imgHandler.occGrid.width,imgHandler.occGrid.height);
        _ptr->publish(occMsg);
    }
    private:
    void _checkParam()
    {
        this->get_parameter("PlannerName",_plannerName);
        if(_lastPlannerName==_plannerName)
        {
            return;
        }
        else
        {
            _lastPlannerName = _plannerName;
            if(_plannerName=="RRT")
            {
                planner.setPlanner(Planner2D::PLANNER_NAMES::RRT);
            }
            else if(_plannerName=="RRTconnect")
            {
                planner.setPlanner(Planner2D::PLANNER_NAMES::RRT_CONNECT);
            }
            else if(_plannerName=="PRM")
            {
                planner.setPlanner(Planner2D::PLANNER_NAMES::PRM);
            }
            else
            {
                RCLCPP_INFO(this->get_logger(),"Unknown planner name");
                return;
            }
            RCLCPP_INFO(this->get_logger(),"Planner has been changed to %s",_plannerName.c_str());
        }
    }
    auto _initPointCallBack(const geometry_msgs::msg::PoseWithCovarianceStamped::SharedPtr msg) -> void
    {
        planner.setStart(msg->pose.pose.position.x,msg->pose.pose.position.y);
        _startPointPtr->publish(_markPoint(msg->pose.pose.position.x,msg->pose.pose.position.y,FrameName));
    }

    auto _finalPointCallBack(const geometry_msgs::msg::PoseStamped::SharedPtr msg)  -> void
    {
        planner.setFinish(msg->pose.position.x,msg->pose.position.y);
        _finishPointPtr->publish(_markPoint(msg->pose.position.x,msg->pose.position.y,FrameName));
        long long _startTime = rclcpp::Clock().now().nanoseconds();
        long long _finishTime = 0;
        if(planner.plan())
        {
            planner.recordSolution();
            PathMsg pathMsg(FrameName,planner.getPathPoints());
            _finishTime = rclcpp::Clock().now().nanoseconds();
            _pathPtr->publish(pathMsg);
            RCLCPP_INFO(this->get_logger(),"Path planning took %f us",((double)(_finishTime-_startTime))/1000);
        }
        else
        {
            RCLCPP_INFO(this->get_logger(),"Can't solve the planning");
        }
        
    }

    geometry_msgs::msg::PointStamped _markPoint(double x,double y,std::string frameID)
    {
        geometry_msgs::msg::PointStamped point;
        point.header.frame_id = frameID;
        point.header.stamp = rclcpp::Clock().now();
        point.point.x = x;
        point.point.y = y;
        point.point.z = 0;
        return point;
    }
    
    rclcpp::TimerBase::SharedPtr _time;

    rclcpp::Publisher<nav_msgs::msg::OccupancyGrid>::SharedPtr _ptr;
    rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr _pathPtr;
    rclcpp::Publisher<geometry_msgs::msg::PointStamped>::SharedPtr _startPointPtr;
    rclcpp::Publisher<geometry_msgs::msg::PointStamped>::SharedPtr _finishPointPtr;

    rclcpp::Subscription<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr _initialPoint;
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr _finalPoint;

    std::string _plannerName = "RRT";
    std::string _lastPlannerName = "RRT";
    rclcpp::TimerBase::SharedPtr _paramTimer;
};


int main(int argc,const char *const argv[])
{
    rclcpp::init(argc,argv);
    rclcpp::spin(std::make_shared<OwnPublisher>());
    rclcpp::shutdown();
    return 0;
}