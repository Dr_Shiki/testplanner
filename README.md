# TestPlanner

Для того, чтобы запустить проект, необходимо
1) Клонировать данный репозиторий
2) Из папки, клонированного проекта (testplanner/) запустить x64 Native Tools Command Promt vs2019
3) Из запущенной консоли запустить local_setup.bat (лежит в корневом каталоге ROS2)
4) Выполнить сборку проекта командой colcon build --symlink
*) Мне потребовалось переместить все чертовы .dll от openCV, ompl, boost и т.д. в папку 			"testplanner\install\planner2D\lib\planner2D"
5) Перейти в подкаталог install 
6) Запустить в текущей консоли local_setup.bat
7) Запустить сам проект ros2 run planner2D planner2D
8) Открыть дополнительное окно cmd в любой папке
9) Повторить для данного окна пункт 3
10) Запустить rviz2 командой ros2 run rviz2 rviz2
11) В GUI rviz2 через инструмент File->OpenConfig открыть planner2DrvizSetup.rviz (в корневом каталоге проекта)

*) требуемые либы на всякий лежат в папке /itCanFix
** imgSource.png - исходное изображение, лежит в корневой папке

