#ifndef IMAGEHANDLER_HEADER
#define IMAGEHANDLER_HEADER

#include <planner2D/Common.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

class ImageHandler
{
public:
    ImageHandler();
    ImageHandler(std::string path,int levelThreshold);
    void getImage(std::string path);
    void thresholdImg(int level);
    OccupancyGrid createGridFromImg();
    std::vector<int8_t> getRowMajorArr();
    std::string imgPath;
    OccupancyGrid occGrid;
private:
    Mat _img;
    Mat _binImg;
};

#endif