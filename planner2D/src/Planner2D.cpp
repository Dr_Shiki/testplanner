#include <planner2D/Planner2D.hpp>

Planner2D::Planner2D(OccupancyGrid occ_Grid)
{
    _occ_Grid = occ_Grid;
    _width = _occ_Grid.width;
    _height = _occ_Grid.height;
    bool ok = true;
    if (ok)
    {
        ompl::msg::noOutputHandler();
        ob::RealVectorStateSpace* space = new ob::RealVectorStateSpace();
        space->addDimension(0.0, _width);
        space->addDimension(0.0, _height);
        maxWidth_ = _width - 1;
        maxHeight_ = _height - 1;
        ss_.reset(new og::SimpleSetup(ob::StateSpacePtr(space)));
        ss_->setStateValidityChecker(boost::bind(&Planner2D::isStateValid, this, _1));
        space->setup();
        ss_->getSpaceInformation()->setStateValidityCheckingResolution(1.0 / space->getMaximumExtent());
        ss_->setPlanner(ob::PlannerPtr(new og::RRT(ss_->getSpaceInformation())));
    }
}

void Planner2D::setPlanner(PLANNER_NAMES name)
{
    if(RRT)
    {
        ss_->setPlanner(ob::PlannerPtr(new og::RRT(ss_->getSpaceInformation())));
    }
    if(RRT_CONNECT)
    {
        ss_->setPlanner(ob::PlannerPtr(new og::RRTConnect(ss_->getSpaceInformation())));
    }
    if(PRM)
    {
        ss_->setPlanner(ob::PlannerPtr(new og::PRM(ss_->getSpaceInformation())));
    }
}

bool Planner2D::plan()
{
    if(!(_isRightTerminantExists&&_isLeftTerminantExists))
        return false;
    if (!ss_)
        return false;
    ob::ScopedState<> start(ss_->getStateSpace());
    start[0] = _start.y;
    start[1] = _start.x;
    ob::ScopedState<> goal(ss_->getStateSpace());
    goal[0] = _finish.y;
    goal[1] = _finish.x;
    ss_->setStartAndGoalStates(start, goal);
    {
        if (ss_->getPlanner())
            ss_->getPlanner()->clear();
            ss_->clear();
            _pathPoints.clear();
            ss_->solve(); 
        }

    const std::size_t ns = ss_->getProblemDefinition()->getSolutionCount();
    if (ss_->haveSolutionPath())
    {
        ss_->simplifySolution();
        og::PathGeometric& p = ss_->getSolutionPath();
        ss_->getPathSimplifier()->simplifyMax(p);
        ss_->getPathSimplifier()->smoothBSpline(p);
        return true;
    }

    return false;
}


void Planner2D::recordSolution()
{
    if (!ss_ || !ss_->haveSolutionPath())
        return;
    og::PathGeometric& p = ss_->getSolutionPath();
    p.interpolate();
    for (std::size_t i = 0; i < p.getStateCount(); ++i)
    {
        const int w = std::min(maxWidth_, (int)p.getState(i)->as<ob::RealVectorStateSpace::StateType>()->values[0]);
        const int h = std::min(maxHeight_, (int)p.getState(i)->as<ob::RealVectorStateSpace::StateType>()->values[1]);
        _point tmp;
        tmp.x = w;
        tmp.y = h;
        _pathPoints.push_back(tmp);
    }
}


std::vector<_point> Planner2D::getPathPoints()
{
    return _pathPoints;
}


void Planner2D::setStart(unsigned int start_row, unsigned int start_col)
{
    _start.x = start_col;
    _start.y = start_row;
    _isRightTerminantExists = true;
}


void Planner2D::setFinish(unsigned int goal_row, unsigned int goal_col)
{
    _finish.x = goal_col;
    _finish.y = goal_row;
    _isLeftTerminantExists = true;
}

    
bool Planner2D::isStateValid(const ob::State* state) const
{
    const int w = std::min((int)state->as<ob::RealVectorStateSpace::StateType>()->values[0], maxWidth_);
    const int h = std::min((int)state->as<ob::RealVectorStateSpace::StateType>()->values[1], maxHeight_);
    return _occ_Grid.grid[h][w]==0;
}



