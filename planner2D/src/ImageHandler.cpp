#include <planner2D/ImageHandler.hpp>

ImageHandler::ImageHandler()
{
}

ImageHandler::ImageHandler(std::string path,int levelThreshold)
{
    imgPath = path;
    getImage(imgPath);
    thresholdImg(levelThreshold);
    createGridFromImg();
}

void ImageHandler::getImage(std::string path)
{
    _img = imread(path);
}

void ImageHandler::thresholdImg(int level)
{
    int thresh = level;
    int threshMax = 255;
    threshold(_img, _binImg, thresh, threshMax, THRESH_BINARY);
}

OccupancyGrid ImageHandler::createGridFromImg()
{
    occGrid.width = _binImg.cols;
    occGrid.height = _binImg.rows;
    occGrid.grid.resize(occGrid.height);

    for(int i=0;i<occGrid.height;i++)
    {
        occGrid.grid[i].resize(occGrid.width);
    }

    for(int i=0;i<occGrid.height;i++)
    {
        for(int j=0;j<occGrid.width;j++)
        {
            occGrid.grid[i][j] = _binImg.at<Vec3b>(i,j)[1]==255?0:100;
        }
    }
    return occGrid;
}

std::vector<int8_t> ImageHandler::getRowMajorArr()
{
    std::vector<int8_t> arr;

    for(int i=0;i<occGrid.height;i++)
    {
        for(int j=0;j<occGrid.width;j++)
        {
            arr.push_back(occGrid.grid[i][j]);
        }
    }
    return arr;
}