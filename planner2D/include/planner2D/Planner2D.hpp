#ifndef PLANNER_HEADER
#define PLANNER_HEADER

#include <planner2D/Common.hpp>

#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/planners/prm/PRM.h>
#include <ompl/util/PPM.h>
#include <ompl/config.h>
#include <boost/filesystem.hpp>
#include <boost/bind.hpp>
#include <string>

using namespace boost;

namespace ob = ompl::base;
namespace og = ompl::geometric;

class Planner2D
{
public:
    enum PLANNER_NAMES{RRT,RRT_CONNECT,PRM};

    Planner2D(OccupancyGrid occ_Grid);

    bool plan();

    void recordSolution();

    std::vector<_point> getPathPoints();

    void setStart(unsigned int start_row, unsigned int start_col);

    void setFinish(unsigned int goal_row, unsigned int goal_col);

    void setPlanner(PLANNER_NAMES name);
private:

    bool isStateValid(const ob::State* state) const;

    og::SimpleSetupPtr ss_;
    int maxWidth_;
    int maxHeight_;
    OccupancyGrid _occ_Grid;
    std::vector<_point> _pathPoints;
    double _width;
    double _height;
    _point _start;
    _point _finish;
    bool _isRightTerminantExists = false;
    bool _isLeftTerminantExists = false;
};

#endif