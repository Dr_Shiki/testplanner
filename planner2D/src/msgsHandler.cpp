#include <planner2D/msgsHandler.hpp>


OccupancyMsg::OccupancyMsg(std::string FrameID,std::vector<int8_t> data,int width,int heigth) : nav_msgs::msg::OccupancyGrid()
{
    header.set__frame_id(FrameID);
    header.set__stamp(rclcpp::Clock().now());
    info.resolution = 1;
    info.width = width;
    info.height = heigth;
    set__data(data);
}

PathMsg::PathMsg(std::string FrameID,std::vector<_point> pathPoints) : nav_msgs::msg::Path()
{
    header.frame_id = FrameID;
    header.stamp = rclcpp::Clock().now();
    set__poses(_convertPointsToData(pathPoints, FrameID));
}

std::vector<geometry_msgs::msg::PoseStamped> PathMsg::_convertPointsToData(std::vector<_point> pathPoints, std::string frameID)
{
    std::vector<geometry_msgs::msg::PoseStamped> PoseStampes;
    for(int i=0;i<pathPoints.size();i++)
    {
        geometry_msgs::msg::PoseStamped tmpPS;

        geometry_msgs::msg::Pose tmpP;
        tmpP.position.x = pathPoints[i].x;
        tmpP.position.y = pathPoints[i].y;
        tmpP.position.z = 0;

        tmpPS.set__pose(tmpP);
        tmpPS.header.frame_id=frameID;
        PoseStampes.push_back(tmpPS);
    }
    return PoseStampes;
}