#ifndef COMMON_HEADER
#define COMMON_HEADER
#include <vector>

typedef std::vector<std::vector<int>> ImgBinGrid;

struct _point
{
    double x;
    double y;
};

struct OccupancyGrid
{
    ImgBinGrid grid;
    double width;
    double height;
};

#endif