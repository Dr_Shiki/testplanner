#ifndef MSGS_HANDLER_HEADER
#define MSGS_HANDLER_HEADER


#include <planner2D/Common.hpp>

#include <rclcpp/rclcpp.hpp>
#include <nav_msgs/msg/occupancy_grid.hpp>
#include <nav_msgs/msg/path.hpp>
#include <geometry_msgs/msg/pose_with_covariance_stamped.hpp>


class OccupancyMsg : public nav_msgs::msg::OccupancyGrid
{
public:
    OccupancyMsg(std::string FrameID,std::vector<int8_t> data,int width,int heigth);
};

class PathMsg : public nav_msgs::msg::Path
{
public:
    PathMsg(std::string FrameID,std::vector<_point> pathPoints);
private:
    std::vector<geometry_msgs::msg::PoseStamped> _convertPointsToData(std::vector<_point> pathPoints, std::string frameID);
};

#endif